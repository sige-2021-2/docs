# Documentação sobre a API Master

## 1\. Visão de Dados 

Devido a model presente no app de measurement ser bem grande, para melhor detalhamento, os arquivos feitos na ferramenta brModelo,
serão colocados na pasta "assets/brmodelo", permitindo possíveis atualização nos diagramas.
A ferramenta brModelo, pode ser baixado no site oficial: [http://www.sis4.com/brModelo/download.html](http://www.sis4.com/brModelo/download.html). Para execução, basta o seguinte comando, java -jar brModelo33.jar.

### 1.1 Diagrama MER(Modelo Entidade Relacionamento)

* Campus

![campus](../assets/images/campus.png)

* Group

![group](../assets/images/group.png)

* Slave

![slave](../assets/images/slave.png)

* Measurement

![measurement](../assets/images/measurement_mer.png)

### 1.2 Diagrama DER(Diagrama Entidade Relacionamento)

* Measurement

![measurement](../assets/images/measurement_der.png)